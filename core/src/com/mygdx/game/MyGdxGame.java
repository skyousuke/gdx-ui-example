package com.mygdx.game;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.NinePatchDrawable;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Disposable;
import com.badlogic.gdx.utils.viewport.ScreenViewport;

public class MyGdxGame extends Game {
    TextureAtlas uiRedAtlas;
    Stage stage;
    BitmapFont font;
    Music currentMusic;

    Slider musicSlider;
    boolean adjustingValue;
    Skin mySkin;

    Array<MusicInfo> musicInfoArray;

    @Override
    public void create() {
        uiRedAtlas = new TextureAtlas("ui-red.atlas");
        stage = new Stage(new ScreenViewport());
        font = new BitmapFont();

        mySkin = new Skin(Gdx.files.internal("myskin.json"), uiRedAtlas);

        musicInfoArray = new Array<MusicInfo>();
        musicInfoArray.add(new MusicInfo("music.mp3", "Kirameku Hamabe", 254));
        musicInfoArray.add(new MusicInfo("music2.mp3", "Desir", 285));
        musicInfoArray.add(new MusicInfo("music3.mp3", "Ignite", 243));

        currentMusic = musicInfoArray.first().music;

        /* create widget */

        // onMusicCompleteListener & pause icon button
        final Button.ButtonStyle playIconStyle = mySkin.get("playIcon", Button.ButtonStyle.class);
        final Button.ButtonStyle pauseIconStyle = mySkin.get("pauseIcon", Button.ButtonStyle.class);
        final Button playIconButton = new Button(playIconStyle);

        // onMusicCompleteListener text button
        final TextButton playTextButton = new TextButton("Play", mySkin);

        // music slider
        musicSlider = new Slider(0, musicInfoArray.first().length, 1, false, mySkin);

        // volume slider
        final Slider volumeSlider = new Slider(0, 1, 0.01f, false, mySkin);
        volumeSlider.setValue(1);
        volumeSlider.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent changeEvent, Actor actor) {
                currentMusic.setVolume(volumeSlider.getValue());
            }
        });

        // music select box
        final SelectBox<String> musicSelectBox = new SelectBox<String>(mySkin);
        musicSelectBox.getScrollPane().setFadeScrollBars(true);
        musicSelectBox.getScrollPane().setFlickScroll(false);
        musicSelectBox.setMaxListCount(2);

        Array<String> names = new Array<String>();
        for (int i = 0; i < musicInfoArray.size; i++) {
            names.add(musicInfoArray.get(i).name);
        }
        musicSelectBox.setItems(names);

        // loop check box
        final CheckBox loopCheckBox = new CheckBox("Looping", mySkin);
        Image image = loopCheckBox.getImage();
        image.setOrigin(image.getWidth() / 2, image.getHeight() / 2);
        image.setScale(0.7f);
        loopCheckBox.getImageCell().size(image.getWidth() * 0.7f, image.getHeight() * 0.7f);
        loopCheckBox.getImageCell().padTop(5);
        loopCheckBox.getImageCell().padRight(5);

        /* set listener */

        // music listener
        final Music.OnCompletionListener onMusicCompleteListener = new Music.OnCompletionListener() {
            @Override
            public void onCompletion(Music music) {
                playTextButton.setText("Play");
                playIconButton.setStyle(playIconStyle);
            }
        };
        currentMusic.setOnCompletionListener(onMusicCompleteListener);

        // onMusicCompleteListener icon button & onMusicCompleteListener text button listener
        final ClickListener playButtonListener = new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                if (!currentMusic.isPlaying()) {
                    playTextButton.setText("Pause");
                    playIconButton.setStyle(pauseIconStyle);
                    currentMusic.play();
                } else {
                    playTextButton.setText("Play");
                    playIconButton.setStyle(playIconStyle);
                    currentMusic.pause();
                }
            }
        };
        playIconButton.addListener(playButtonListener);
        playTextButton.addListener(playButtonListener);

        // music select box listener
        musicSelectBox.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                changeMusic(musicSelectBox.getSelected(), playTextButton, playIconButton, playIconStyle,
                        loopCheckBox, onMusicCompleteListener);
            }
        });

        // music slider listener
        musicSlider.addListener(new InputListener() {
            boolean playingBeforeTouchDown;

            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                adjustingValue = true;
                playingBeforeTouchDown = currentMusic.isPlaying();
                if (playingBeforeTouchDown)
                    currentMusic.pause();
                return true;
            }

            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                currentMusic.play();
                currentMusic.setPosition(musicSlider.getValue());
                currentMusic.pause();

                if (playingBeforeTouchDown)
                    currentMusic.play();
                adjustingValue = false;
            }
        });

        // loop check box listener
        loopCheckBox.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                currentMusic.setLooping(loopCheckBox.isChecked());
            }
        });

        // Window
        Window.WindowStyle windowStyle = new Window.WindowStyle();
        windowStyle.titleFont = font;
        windowStyle.background = new NinePatchDrawable(uiRedAtlas.createPatch("window_01"));
        final Window window = new Window("Music Player", windowStyle);

        window.pad(20);
        window.padTop(40);
        window.row().padTop(20);
        window.add(musicSelectBox).width(200).colspan(2).expandX();
        window.row().padTop(10);
        window.add(musicSlider).width(300).colspan(2);
        window.row().padTop(10).uniform();
        window.add(playIconButton).right();
        window.add(playTextButton).padLeft(10).left();
        window.row().padTop(10);

        Table volumeTable = new Table();
        Label.LabelStyle labelStyle = new Label.LabelStyle(font, Color.BLACK);
        volumeTable.add(new Label("Volume", labelStyle));
        volumeTable.add(volumeSlider).expandX().fillX().padLeft(10);

        window.add(volumeTable).colspan(2).fillX();
        window.row().padTop(10);
        window.add(loopCheckBox).colspan(2);
        window.pack();

        stage.addActor(window);

        Gdx.input.setInputProcessor(stage);
    }

    @Override
    public void render() {
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        if (!adjustingValue)
            updateMusicProgress();

        stage.act();
        stage.draw();
    }

    private void updateMusicProgress() {
        musicSlider.setValue(currentMusic.getPosition());
    }

    private void changeMusic(String musicName, TextButton playTextButton, Button playIconButton,
                             Button.ButtonStyle playIconStyle, CheckBox loopCheckBox,
                             Music.OnCompletionListener onMusicCompleteListener) {
        currentMusic.stop();
        currentMusic.setOnCompletionListener(null);

        MusicInfo newMusic = null;
        for (int i = 0; i < musicInfoArray.size; i++) {
            if (musicInfoArray.get(i).name.equals(musicName)) {
                newMusic = musicInfoArray.get(i);
                break;
            }
        }
        if (newMusic != null) {
            currentMusic = newMusic.music;
            currentMusic.setLooping(loopCheckBox.isChecked());
            currentMusic.setOnCompletionListener(onMusicCompleteListener);
            playTextButton.setText("Play");
            playIconButton.setStyle(playIconStyle);
            musicSlider.setRange(0, newMusic.length);
            musicSlider.setValue(0);
        }
    }

    @Override
    public void resize(int width, int height) {
        stage.getViewport().update(width, height);
    }

    @Override
    public void dispose() {
        stage.dispose();
        uiRedAtlas.dispose();
        font.dispose();
        currentMusic.dispose();
        for (int i = 0; i < musicInfoArray.size; i++) {
            musicInfoArray.get(i).dispose();
        }
        mySkin.dispose();
    }

    class MusicInfo implements Disposable {
        Music music;
        String name;
        int length;

        public MusicInfo(String internalFile, String name, int length) {
            this.music = Gdx.audio.newMusic(Gdx.files.internal(internalFile));
            this.name = name;
            this.length = length;
        }

        @Override
        public void dispose() {
            music.dispose();
        }
    }
}
